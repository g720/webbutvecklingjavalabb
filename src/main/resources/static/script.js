const handleDelete = (id) => {
    fetch(`/links/${id}`, {
        method: 'DELETE',
    }).then((res) => {
        window.location.href = res.url
    })
}

const handleCategoryClick = (category) => {
    if(category){
        window.location.href = `/links/?category=${category}`;
    } else {
        window.location.href = `/links`;
    }
}