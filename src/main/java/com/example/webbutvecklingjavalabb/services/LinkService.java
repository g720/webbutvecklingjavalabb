package com.example.webbutvecklingjavalabb.services;

import com.example.webbutvecklingjavalabb.entities.Link;
import org.springframework.stereotype.Service;

import java.util.List;


public interface LinkService {

    public void saveLink(Link link);

    public void deleteLinkById(int id);

    public List<Link> getAllLinks(String category);

    public List<String> findAllCategories();

}
