package com.example.webbutvecklingjavalabb.services;

import com.example.webbutvecklingjavalabb.entities.Link;
import com.example.webbutvecklingjavalabb.repositories.LinkRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LinkServiceImpl implements LinkService {

    LinkRepository linkRepository;

    public LinkServiceImpl(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @Override
    public void saveLink(Link link) {
        linkRepository.save(link);
    }

    @Override
    public void deleteLinkById(int id) {
        linkRepository.deleteById(id);
    }

    @Override
    public List<Link> getAllLinks(String category) {

        if(category != null){
            return linkRepository.findAllByCategory(category);
        }else{
            return linkRepository.findAll();
        }
    }

    @Override
    public List<String> findAllCategories() {
        return linkRepository.findCategoryGroups();
    }
}
