package com.example.webbutvecklingjavalabb.repositories;

import com.example.webbutvecklingjavalabb.entities.Link;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LinkRepository extends JpaRepository<Link, Integer> {

    @Query(value =
    "SELECT CATEGORY FROM LINK GROUP BY CATEGORY", nativeQuery = true
    )
    List<String> findCategoryGroups();

    List<Link> findAllByCategory(String category);
}
