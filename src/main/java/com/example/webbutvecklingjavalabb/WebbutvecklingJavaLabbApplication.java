package com.example.webbutvecklingjavalabb;

import com.example.webbutvecklingjavalabb.entities.Link;
import com.example.webbutvecklingjavalabb.repositories.LinkRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WebbutvecklingJavaLabbApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebbutvecklingJavaLabbApplication.class, args);}

    @Bean
    CommandLineRunner init(LinkRepository linkRepository){
        return args -> {
            Link link = new Link("https://www.google.se", "Google", "Bästa sökmotorn", "Sökmotorer");
            linkRepository.save(link);
            Link link1 = new Link("https://app.diagrams.net/", "Diagrams", "Skapa diagram", "Tools");
            linkRepository.save(link1);
            Link link2 = new Link("https://pastebin.com/", "Pastebin", "Spara och skicka kod", "Tools");
            linkRepository.save(link2);
            Link link3 = new Link("https://codepen.io/trending", "CodePen", "Testa din html/css & js", "Tools");
            linkRepository.save(link3);
            Link link4 = new Link("https://www.bing.se", "Bing", "TryHardFails", "Sökmotorer");
            linkRepository.save(link4);
            Link link5 = new Link("https://www.w3schools.com/sql/trysql.asp?filename=trysql_asc", "SQL TryIt", "Testa dina querys", "Tools");
            linkRepository.save(link5);
            Link link6 = new Link("https://www.ica.se/recept/vegetarisk-stroganoff-716672/", "Stroganoff", "Matig och vegetarisk stroganoff", "Recept");
            linkRepository.save(link6);
            Link link7 = new Link("https://tyngre.se/recept/chokladbollssemla/", "Semla", "Perfekt fika", "Recept");
            linkRepository.save(link7);
            Link link8 = new Link("https://www.ica.se/recept/avokado-wraps-723005/", "Wrap", "Ogod wrap", "Recept");
            linkRepository.save(link8);
            Link link9 = new Link("https://www.arla.se/recept/penne-med-svamp-och-pinjenotter/", "Penne", "God pasta", "Recept");
            linkRepository.save(link9);
        };
    }

}
