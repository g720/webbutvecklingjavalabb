package com.example.webbutvecklingjavalabb.controllers;

import com.example.webbutvecklingjavalabb.entities.Link;
import com.example.webbutvecklingjavalabb.services.LinkService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/links")
public class LinkController {

    private final LinkService linkService;

    public LinkController(LinkService linkService) {
        this.linkService = linkService;
    }


    @GetMapping()
    public String getAllLinks(Model model,
                              @RequestParam(required = false) String category){
        List<Link> linkList = linkService.getAllLinks(category);
        model.addAttribute("linkList", linkList);
        List<String> categoryList = linkService.findAllCategories();
        model.addAttribute("categoryList", categoryList);
        return "link";
    }


    @GetMapping("/add")
    public String getAddLinkForm(){
        return "addLink";
    }

    @PostMapping("/add")
    public String addLink(@ModelAttribute Link link){
        linkService.saveLink(link);
        return "redirect:/links";
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteLinkById(@PathVariable("id") int id){
        linkService.deleteLinkById(id);
        return ResponseEntity.status(303).header("Location", "/links").build();
    }



}
